import React from "react";
import propTypes from "prop-types";
import Row from "./table/row";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as row from "../redux/actions/incomeAction";
import NumberFormat from 'react-number-format';

const Income = props => {
  return (
    <div className="mb-5">
      <div className="card-header" id="headingOne">
        <div className="d-flex bd-highlight">
          <div className="mr-auto p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong>Income</strong>
            </div>
          </div>
          <div className="p-2 bd-highlight">
            <div className="h4 cHeader pointer" onClick={props.addRow}>
              <ion-icon name="add" />
              <strong>Add Row</strong>
            </div>
          </div>
        </div>
      </div>
      <table className="table table-borderless">
        <thead className="tHeader">
          <tr>
            <th />
            <th scope="col">INCOME</th>
            <th scope="col">AMOUNT</th>
            <th scope="col">
              <div className="text-right">AMOUNT</div>
            </th>
          </tr>
        </thead>
        <tbody>
          {props.row.map((val, idx) => (
            <Row
              key={idx}
              remove={props.removeRow.bind(idx)}
              change={e => props.handleChange(e, idx)}
              value={val}
            />
          ))}
        </tbody>
      </table>
      <div className="card-header" id="headingOne">
        <div className="d-flex bd-highlight">
          <div className="mr-auto p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong>Total Income :</strong>
            </div>
          </div>
          <div className="p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong><NumberFormat value={props.total} displayType={'text'} thousandSeparator={true} prefix={'$'} /></strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateProps = state => ({
  row: state.income.row,
  input: state.income.input,
  total: state.income.total
});

function mapDispatcheProps(dispatch) {
  return bindActionCreators(row, dispatch);
}

Income.propTypes = {
  total: propTypes.number,
  handleChange: propTypes.func
};

export default connect(
  mapStateProps,
  mapDispatcheProps
)(Income);
