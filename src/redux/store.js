import { createStore, combineReducers } from "redux";
import expenseReducer from "./reducers/expenseReducer";
import incomeReducer from "./reducers/incomeReducer";

let rootReducer = combineReducers({
  income: incomeReducer,
  expense: expenseReducer
});

const Store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default Store;
