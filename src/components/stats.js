import React from "react";
import { connect } from "react-redux";
import NumberFormat from 'react-number-format';

const Stats = props => {
  const amtLeft = () => {
    let amtLeft = props.income - props.expense;
    return amtLeft.toFixed(2);
  };
  return (
    <div className="d-flex justify-content-center text-center">
      <div className="m-2">
        <strong>Total Income</strong>
        <p><NumberFormat value={props.income} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
      </div>
      <div className="m-2">
        <strong>Total Expenses</strong>
        <p><NumberFormat value={props.expense} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
      </div>
      <div className="m-2">
        <strong>Amount Left</strong>
        <p><NumberFormat value={amtLeft()} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
      </div>
    </div>
  );
};

function mapStateProps(state) {
  return {
    income: state.income.total,
    expense: state.expense.total
  };
}

export default connect(mapStateProps)(Stats);
