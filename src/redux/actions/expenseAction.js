import {
  ADD_ROW_EXPENSE,
  REMOVE_ROW_EXPENSE,
  HANDLE_CHANGE_EXPENSE
} from "./types";
export const addRowE = idx => {
  return {
    type: ADD_ROW_EXPENSE,
    idx
  };
};

export const removeRowE = idx => {
  return {
    type: REMOVE_ROW_EXPENSE,
    idx
  };
};

export const handleChangeE = (e, idx) => {
  return {
    type: HANDLE_CHANGE_EXPENSE,
    e,
    idx
  };
};
