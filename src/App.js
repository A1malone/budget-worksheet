import React, { Component } from "react";
import Income from "./components/income";
import Expense from "./components/expense";
import Chart from "./components/chart";
import Stats from "./components/stats";
import { Provider } from "react-redux";
import Store from "./redux/store";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <div className="container-fluid">
          <h1 className="text-center mt-3">Budget Calculator</h1>
          <div className="row m-5 ">
            <div className="col-lg-6">
              <Income />
              <div className="m-5" />
              <div className="m-5" />
              <Expense />
            </div>
            <div className="col-lg-6">
              <Chart />
              <Stats />
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
