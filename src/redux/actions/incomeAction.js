import {
  ADD_ROW_INCOME,
  REMOVE_ROW_INCOME,
  HANDLE_CHANGE_INCOME
} from "./types";

export const addRow = idx => {
  return {
    type: ADD_ROW_INCOME,
    idx
  };
};

export const removeRow = idx => {
  return {
    type: REMOVE_ROW_INCOME,
    idx
  };
};

export const handleChange = (e, idx) => {
  return {
    type: HANDLE_CHANGE_INCOME,
    e,
    idx
  };
};
