import React from "react";
import NumberFormat from 'react-number-format';
const Row = props => {
  return (
    <tr>
      {/* removed <th scope="row">1</th> to fix responsivenes via mobile */}
      <td>
        <div className="center-row-action pointer">
          <ion-icon name="close-circle" onClick={props.remove} />
        </div>
      </td>
      <td>
        <input
          type="text"
          className="form-control"
          name="source"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-sm"
          placeholder="Source"
        />
      </td>
      <td>
        <div className="input-group input-group mb-3 text-center">
        {/*<NumberFormat className="form-control" onChange={props.change}  value={props.value} thousandSeparator={true}  />*/}
          <input
            type="text"
            className="form-control"
            name="price"
            onChange={props.change}
            value={props.value}
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-sm"
            placeholder="$0.00"
          /> 
          {/*
          
          <select className="custom-select" id="inputGroupSelect01">
            <option value="selected">Choose...</option>
            <option value="1">Daily</option>
            <option value="2">Weekly</option>
            <option value="3">Bi-Weekly</option>
            <option value="3">Monthly</option>
            <option value="3">Annually</option>
            <option value="3">One Time</option>
          </select>
           */}
        </div>
      </td>
      <td className="d-flex justify-content-end"><NumberFormat value={props.value} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>
    </tr>
  );
};

export default Row;
