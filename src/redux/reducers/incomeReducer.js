import {
  ADD_ROW_INCOME,
  REMOVE_ROW_INCOME,
  HANDLE_CHANGE_INCOME
} from "../actions/types";

const initialState = {
  row: [0],
  total: 0,
  input: 0
};

export default function incomeReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_ROW_INCOME:
      return { ...state, row: [...state.row, 0] };
    case REMOVE_ROW_INCOME:
      return {
        ...state,
        row: [...state.row.slice(0, -1)]
      };
    case HANDLE_CHANGE_INCOME:
      // access value
      let rows = [...state.row];

      rows[action.idx] = action.e.target.value;

      //accumulator for total
      let total = rows.reduce(
        (accumulator, currentValue) =>
          Number(accumulator) + Number(currentValue)
      );
      return {
        ...state,
        row: rows,
        total: Number(total),
        input: rows[action.idx]
      };
    default:
      return state;
  }
}
