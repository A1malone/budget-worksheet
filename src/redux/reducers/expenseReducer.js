import {
  ADD_ROW_EXPENSE,
  REMOVE_ROW_EXPENSE,
  HANDLE_CHANGE_EXPENSE
} from "../actions/types";

const initialState = {
  row: [0],
  total: 0,
  input: 0
};

export default function expenseReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_ROW_EXPENSE:
      return { ...state, row: [...state.row, 0] };
    case REMOVE_ROW_EXPENSE:
      return {
        ...state,
        row: [...state.row.slice(0, -1)]
      };
    case HANDLE_CHANGE_EXPENSE:
      // access value
      let rows = [...state.row];

      rows[action.idx] = action.e.target.value;

      //accumulator for total
      let total = rows.reduce(
        (accumulator, currentValue) =>
          Number(accumulator) + Number(currentValue)
      );
      return {
        ...state,
        row: rows,
        total: Number(total),
        input: rows[action.idx]
      };
    //Object.assign({}, state, { total: action.total.income });
    default:
      return state;
  }
}
