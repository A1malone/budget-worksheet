import React, { Component } from "react";
import Row from "./row.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as row from "../../redux/actions/rowAction";
import "./table.css";
class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [0.0],
      total: [0.0]
    };
  }
  handleChange = (e, idx) => {
    // access value
    let rows = this.state.rows;
    rows[idx] = e.target.value;

    //accumulator for total
    let amount = this.state.rows;
    let total = amount.reduce(
      (accumulator, currentValue) => Number(accumulator) + Number(currentValue)
    );

    this.setState({ rows: rows, total: total });
  };

  addRow = () => {
    this.setState({ rows: [...this.state.rows, 0] });
  };
  removeRow = () => {
    this.setState({
      rows: this.state.rows.slice(0, -1)
    });
  };

  render() {
    let { rows } = this.state;
    return (
      <div className="mb-5">
        <div className="card-header" id="headingOne">
          <div className="d-flex bd-highlight">
            <div className="mr-auto p-2 bd-highlight">
              <div className="h4 cHeader">
                <strong>{this.props.type}</strong>
              </div>
            </div>
            <div className="p-2 bd-highlight">
              <div className="h4 cHeader" onClick={this.addRow}>
                <ion-icon name="add" />
                <strong>Add Row</strong>
              </div>
            </div>
          </div>
        </div>
        <table className="table table-borderless">
          <thead className="tHeader">
            <tr>
              <th />
              <th scope="col">INCOME</th>
              <th scope="col">AMOUNT / FREQUENCY</th>
              <th scope="col">
                <div className="text-right">AMOUNT</div>
              </th>
            </tr>
          </thead>
          <tbody>
            {rows.map((val, idx) => (
              <Row
                key={idx}
                remove={this.removeRow}
                change={e => this.handleChange(e, idx)}
                value={val}
              />
            ))}
          </tbody>
        </table>
        <div className="card-header" id="headingOne">
          <div className="d-flex bd-highlight">
            <div className="mr-auto p-2 bd-highlight">
              <div className="h4 cHeader">
                <strong>Total {this.props.type} :</strong>
              </div>
            </div>
            <div className="p-2 bd-highlight">
              <div className="h4 cHeader">
                <strong>${this.state.total}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateProps(state) {
  return {
    row: state.row,
    income: {
      row: state.row
    }
  };
}

function mapDispatcheProps(dispatch) {
  return bindActionCreators(row, dispatch);
}

export default connect(
  mapStateProps,
  mapDispatcheProps
)(Table);
