# Budget Worksheet

This is a simple To-Do list turned into a way of keeping track of your budget.

view [HERE](https://alfredmalone.com/budget/)

## Features

- Calculates total income and expenses.
- Uses frequency (EX: Monthly, Bi-Weekly, Annually).
- Displays data in a chart to give a visual understanding of your budget.
