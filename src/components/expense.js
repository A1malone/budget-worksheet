import React from "react";
import Row from "./table/row";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as row from "../redux/actions/expenseAction";
import NumberFormat from 'react-number-format';


const Expense = props => {
  return (
    <div className="mb-5">
      <div className="card-header" id="headingOne">
        <div className="d-flex bd-highlight">
          <div className="mr-auto p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong>Expense</strong>
            </div>
          </div>
          <div className="p-2 bd-highlight">
            <div className="h4 cHeader pointer" onClick={props.addRowE}>
              <ion-icon name="add" />
              <strong>Add Row</strong>
            </div>
          </div>
        </div>
      </div>
      <table className="table table-borderless">
        <thead className="tHeader">
          <tr>
            <th />
            <th scope="col">INCOME</th>
            <th scope="col">AMOUNT</th>
            <th scope="col">
              <div className="text-right">AMOUNT</div>
            </th>
          </tr>
        </thead>
        <tbody>
          {props.row.map((val, idx) => (
            <Row
              key={idx}
              remove={props.removeRowE.bind(idx)}
              change={e => props.handleChangeE(e, idx)}
              value={val}
            />
          ))}
        </tbody>
      </table>
      <div className="card-header" id="headingOne">
        <div className="d-flex bd-highlight">
          <div className="mr-auto p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong>Total Expense :</strong>
            </div>
          </div>
          <div className="p-2 bd-highlight">
            <div className="h4 cHeader">
              <strong><NumberFormat value={props.total} displayType={'text'} thousandSeparator={true} prefix={'$'} /></strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateProps = state => ({
  row: state.expense.row,
  input: state.expense.input,
  total: state.expense.total
});

function mapDispatcheProps(dispatch) {
  return bindActionCreators(row, dispatch);
}

export default connect(
  mapStateProps,
  mapDispatcheProps
)(Expense);
