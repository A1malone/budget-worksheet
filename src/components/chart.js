import React from "react";
import { Pie } from "react-chartjs-2";
import { connect } from "react-redux";

const Chart = props => {
  let data = {
    labels: ["Income", "Expense"],
    datasets: [
      {
        fill: false,
        lineTension: 0.1,
        backgroundColor: ["rgba(79,255,4,0.90)", "rgba(248, 42, 42, 0.90)"],
        borderColor: "#141414",
        data: [props.income, props.expense]
      }
    ]
  };

  return (
    <div>
      <Pie
        data={data}
        options={{
          legend: {
            display: true,
            labels: {
              fontColor: "#fff"
            }
          },
          tooltips: { intersect: false }
        }}
      />
    </div>
  );
};
function mapStateProps(state) {
  return {
    income: state.income.total,
    expense: state.expense.total
  };
}

export default connect(mapStateProps)(Chart);
